<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Stock extends Model{
    public $id;
    public $id_producto;
    public $cantidad;
    public $fecha;

    public function rules()
    {
        return [
            ['cantidad', 'required', 'message'=>'El nombre es requerido'],
            ['cantidad', "number", 'message'=>'Solo numeros'],
            ['id_producto', 'required', 'message'=>'El nombre es requerido'],
            ['id_producto', "number", 'message'=>'Solo numeros'],
            ['fecha', 'required', 'message'=>'El nombre es requerido']
        ];

    }

    public function attributeLabels()
    {
        return [
            'id'=> 'ID :',
            'id_producto'=> 'ID Producto:',
            'fecha'=> 'Fecha:',
            'cantidad'=> 'Cantidad:'
        ];
    }

}

?>