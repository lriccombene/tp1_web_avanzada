<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "seguimiento_tramite".
 *
 * @property int $id
 * @property int $id_nota_expet
 * @property int $id_destinatario
 * @property string $estado_tramite
 * @property string $fecha_pase
 */
class SeguimientoTramite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seguimiento_tramite';
    }

    /**
     * {@inheritdoc}
     * [['id_nota_expet', 'id_destinatario', 'estado_tramite', 'fecha_pase'], 'required'],
     */
    public function rules()
    {
        return [
            [[ 'fecha_pase'], 'required'],
            [['id_nota_expet', 'id_destinatario'], 'integer'],
            [['fecha_pase'], 'safe'],
            [['estado_tramite'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_nota_expet' => 'Nota Expet',
            'id_destinatario' => 'Destinatario',
            'estado_tramite' => 'Estado Tramite',
            'fecha_pase' => 'Fecha Pase',
        ];
    }
}
