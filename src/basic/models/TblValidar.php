<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;


class TblValidar extends ActiveRecord{

    public static function getDb()
    {
        return Yii::$app->db;
    }

    public static function tableName()
    {
        return 'usuario';
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->nombre = $this->nombre." OK";
        return true;
    }
}



?>
