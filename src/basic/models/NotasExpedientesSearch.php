<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NotasExpedientes;

/**
 * NotasExpedientesSearch represents the model behind the search form of `app\models\NotasExpedientes`.
 */
class NotasExpedientesSearch extends NotasExpedientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nro_tramite', 'estado', 'remitente'], 'integer'],
            [['tramite_original', 'nro_expte_nota', 'fec_expt', 'fec_llegada_viedma', 'caratula_expte_nuevo', 'fec_ingreso', 'fojas', 'resumen', 'observaciones'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NotasExpedientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nro_tramite' => $this->nro_tramite,
            'estado' => $this->estado,
            'fec_expt' => $this->fec_expt,
            'fec_llegada_viedma' => $this->fec_llegada_viedma,
            'fec_ingreso' => $this->fec_ingreso,
            'remitente' => $this->remitente,
        ]);

        $query->andFilterWhere(['like', 'tramite_original', $this->tramite_original])
            ->andFilterWhere(['like', 'nro_expte_nota', $this->nro_expte_nota])
            ->andFilterWhere(['like', 'caratula_expte_nuevo', $this->caratula_expte_nuevo])
            ->andFilterWhere(['like', 'fojas', $this->fojas])
            ->andFilterWhere(['like', 'resumen', $this->resumen])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones]);

        return $dataProvider;
    }
}
