<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Validar extends Model{
    public $id;
    public $nombre;
    public $email;

    public function rules()
    {
        return [
            ['nombre', 'required', 'message'=>'El nombre es requerido'],
            ['nombre', 'match', 'pattern'=>"/^.{3,50}$/", 'message'=>'Debe ser de 3 a 50 caracteres'],
            ['nombre', 'match', 'pattern'=>"/^[0-9a-z]+$/i", 'message'=>'Solo letras y numeros'],
            ['email', 'required', 'message'=>"El email es requerido"],
            ['email', 'email', 'message'=>'Email no válido'],
            ['email', 'email_exists']
        ];

    }

    public function email_exists($attribute, $params)
    {
        //Query, calculo, etc
        $emails = ['jperez@gmail.com', 'pepe@hotmail.com'];
        foreach ($emails as $mail){
            if ($this->email == $mail)
            {
                $this->addError($attribute, "El correo ya existe.");
                return true;
            }
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Código',
            'nombre'=> 'Nombre:',
            'email'=> 'Email:'
        ];
    }

}

?>
