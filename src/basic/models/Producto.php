<?php
namespace app\models;

use Yii;
use yii\base\Model;

class Producto extends Model{
    public $id;
    public $nombre;
    public $codigo;
    public $detalle;
    public $cantidad;
    
    public function rules()
    {
        return [
            ['nombre', 'required', 'message'=>'El nombre es requerido'],
            ['nombre', 'match', 'pattern'=>"/^.{3,50}$/", 'message'=>'Debe ser de 3 a 50 caracteres'],
            ['nombre', 'match', 'pattern'=>"/^[0-9a-z]+$/i", 'message'=>'Solo letras y numeros'],
            ['detalle', 'match', 'pattern'=>"/^.{3,50}$/", 'message'=>'Debe ser de 3 a 50 caracteres'],
            ['codigo', "codigo_existe"],
            ['cantidad', "number", 'message'=>'Solo numeros']
        ];

    }

    public function codigo_existe($attribute, $params)
    {
        //Query, calculo, etc
        $codigos = ['666', '123'];
        foreach ($codigos as $codigo){
            if ($this->codigo == $codigo)
            {
                $this->addError($attribute, "El correo ya existe.");
                return true;
            }
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'id'=> 'ID:',
            'codigo'=> 'Codigo:',
            'detalle'=> 'Detalle:',
            'nombre'=> 'Nombre:',
            'cantidad'=> 'Cantidad:'
        ];
    }

}

?>