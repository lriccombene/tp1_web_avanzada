<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EstudioImpactoAmbiental;

/**
 * EstudioImpactoAmbientalSearch represents the model behind the search form of `app\models\EstudioImpactoAmbiental`.
 */
class EstudioImpactoAmbientalSearch extends EstudioImpactoAmbiental
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reglon', 'fojas_recepcion', 'ultimo_tramite', 'ult_usu'], 'integer'],
            [['tipo_eia', 'empresa', 'referencia', 'consultor', 'localidad', 'nro_expte_mineria', 'fec_recepcion', 'tipo_expediente', 'sub_tipo_expediente', 'expediente_sma', 'mineral', 'fec_nota', 'numero_Nota', 'nombre_area', 'nombre_yacimiento', 'nombre_proyecto', 'arrendatario', 'observaciones', 'revision', 'secto_Revision', 'fec_ultimo_pase'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstudioImpactoAmbiental::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reglon' => $this->reglon,
            'fec_recepcion' => $this->fec_recepcion,
            'fec_nota' => $this->fec_nota,
            'fojas_recepcion' => $this->fojas_recepcion,
            'revision' => $this->revision,
            'fec_ultimo_pase' => $this->fec_ultimo_pase,
            'ultimo_tramite' => $this->ultimo_tramite,
            'ult_usu' => $this->ult_usu,
        ]);

        $query->andFilterWhere(['like', 'tipo_eia', $this->tipo_eia])
            ->andFilterWhere(['like', 'empresa', $this->empresa])
            ->andFilterWhere(['like', 'referencia', $this->referencia])
            ->andFilterWhere(['like', 'consultor', $this->consultor])
            ->andFilterWhere(['like', 'localidad', $this->localidad])
            ->andFilterWhere(['like', 'nro_expte_mineria', $this->nro_expte_mineria])
            ->andFilterWhere(['like', 'tipo_expediente', $this->tipo_expediente])
            ->andFilterWhere(['like', 'sub_tipo_expediente', $this->sub_tipo_expediente])
            ->andFilterWhere(['like', 'expediente_sma', $this->expediente_sma])
            ->andFilterWhere(['like', 'mineral', $this->mineral])
            ->andFilterWhere(['like', 'numero_Nota', $this->numero_Nota])
            ->andFilterWhere(['like', 'nombre_area', $this->nombre_area])
            ->andFilterWhere(['like', 'nombre_yacimiento', $this->nombre_yacimiento])
            ->andFilterWhere(['like', 'nombre_proyecto', $this->nombre_proyecto])
            ->andFilterWhere(['like', 'arrendatario', $this->arrendatario])
            ->andFilterWhere(['like', 'observaciones', $this->observaciones])
            ->andFilterWhere(['like', 'secto_Revision', $this->secto_Revision]);

        return $dataProvider;
    }
}
