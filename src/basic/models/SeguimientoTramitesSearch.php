<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SeguimientoTramite;

/**
 * SeguimientoTramitesSearch represents the model behind the search form of `app\models\SeguimientoTramite`.
 */
class SeguimientoTramitesSearch extends SeguimientoTramite
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_nota_expet', 'id_destinatario'], 'integer'],
            [['estado_tramite', 'fecha_pase'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SeguimientoTramite::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_nota_expet' => $this->id_nota_expet,
            'id_destinatario' => $this->id_destinatario,
            'fecha_pase' => $this->fecha_pase,
        ]);

        $query->andFilterWhere(['like', 'estado_tramite', $this->estado_tramite]);

        return $dataProvider;
    }
}
