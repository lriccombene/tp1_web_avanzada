<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudio_impacto_ambiental".
 *
 * @property int $id
 * @property int $reglon
 * @property string $tipo_eia
 * @property string $empresa
 * @property string $referencia
 * @property string $consultor
 * @property string $localidad
 * @property string $nro_expte_mineria
 * @property string $fec_recepcion
 * @property string $tipo_expediente
 * @property string $sub_tipo_expediente
 * @property string $expediente_sma
 * @property string $mineral
 * @property string $fec_nota
 * @property string $numero_Nota
 * @property int $fojas_recepcion
 * @property string $nombre_area
 * @property string $nombre_yacimiento
 * @property string $nombre_proyecto
 * @property string $arrendatario
 * @property string $observaciones
 * @property string $revision
 * @property string $secto_Revision
 * @property string $fec_ultimo_pase
 * @property int $ultimo_tramite
 * @property int $ult_usu
 */
class EstudioImpactoAmbiental extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudio_impacto_ambiental';
    }

    /**
     * {@inheritdoc}
     * 'tipo_eia', 'empresa', 'referencia', 'consultor', 'localidad', 'nro_expte_mineria', 'fec_recepcion', 'tipo_expediente', 'sub_tipo_expediente', 'expediente_sma', 'mineral', 'fec_nota', 'numero_Nota', 'fojas_recepcion', 'nombre_area', 'nombre_yacimiento', 'nombre_proyecto', 'arrendatario', 'observaciones', 'revision', 'secto_Revision', 'fec_ultimo_pase', 'ultimo_tramite', 'ult_usu'
     */
    public function rules()
    {
        return [
            [['reglon' ], 'required'],
            [['reglon', 'fojas_recepcion', 'ultimo_tramite', 'ult_usu'], 'integer'],
            [['fec_recepcion', 'fec_nota', 'revision', 'fec_ultimo_pase'], 'safe'],
            [['observaciones'], 'string'],
            [['tipo_eia', 'localidad', 'expediente_sma', 'nombre_area'], 'string', 'max' => 150],
            [['empresa'], 'string', 'max' => 300],
            [['referencia'], 'string', 'max' => 500],
            [['consultor', 'nombre_yacimiento', 'nombre_proyecto', 'arrendatario'], 'string', 'max' => 200],
            [['nro_expte_mineria'], 'string', 'max' => 20],
            [['tipo_expediente', 'sub_tipo_expediente', 'mineral', 'secto_Revision'], 'string', 'max' => 100],
            [['numero_Nota'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reglon' => 'Reglon',
            'tipo_eia' => 'Tipo Eia',
            'empresa' => 'Empresa',
            'referencia' => 'Referencia',
            'consultor' => 'Consultor',
            'localidad' => 'Localidad',
            'nro_expte_mineria' => 'Nro Expte Mineria',
            'fec_recepcion' => 'Fec Recepcion',
            'tipo_expediente' => 'Tipo Expediente',
            'sub_tipo_expediente' => 'Sub Tipo Expediente',
            'expediente_sma' => 'Expediente Sma',
            'mineral' => 'Mineral',
            'fec_nota' => 'Fec Nota',
            'numero_Nota' => 'Numero Nota',
            'fojas_recepcion' => 'Fojas Recepcion',
            'nombre_area' => 'Nombre Area',
            'nombre_yacimiento' => 'Nombre Yacimiento',
            'nombre_proyecto' => 'Nombre Proyecto',
            'arrendatario' => 'Arrendatario',
            'observaciones' => 'Observaciones',
            'revision' => 'Revision',
            'secto_Revision' => 'Secto Revision',
            'fec_ultimo_pase' => 'Fec Ultimo Pase',
            'ultimo_tramite' => 'Ultimo Tramite',
            'ult_usu' => 'Ult Usu',
        ];
    }
}
