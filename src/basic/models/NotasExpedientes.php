<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notas_expedientes".
 *
 * @property int $id
 * @property int $nro_tramite
 * @property string $tramite_original
 * @property int $estado
 * @property string $nro_expte_nota
 * @property string $fec_expt
 * @property string $fec_llegada_viedma
 * @property string $caratula_expte_nuevo
 * @property string $fec_ingreso
 * @property string $fojas
 * @property string $resumen
 * @property int $remitente
 * @property string $observaciones
 */
class NotasExpedientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notas_expedientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        //'tramite_original', 'estado', 'nro_expte_nota', 'fec_expt', 'fec_llegada_viedma', 'caratula_expte_nuevo', 'fec_ingreso', 'fojas', 'resumen', 'remitente', 'observaciones'], 'required'],
        return [
            [['nro_tramite'], 'required'], 
            [['nro_tramite', 'estado', 'remitente'], 'integer'],
            [['fec_expt', 'fec_llegada_viedma', 'fec_ingreso'], 'safe'],
            [['resumen', 'observaciones'], 'string'],
            [['tramite_original', 'nro_expte_nota', 'fojas'], 'string', 'max' => 50],
            [['caratula_expte_nuevo'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'nro_tramite' => 'Nro Tramite :',
            'tramite_original' => 'Tramite Original :',
            'estado' => 'Estado :',
            'nro_expte_nota' => 'Nro Expte Nota :',
            'fec_expt' => 'Fec Expt :',
            'fec_llegada_viedma' => 'Fec Llegada Viedma :',
            'caratula_expte_nuevo' => 'Caratula Expte Nuevo :',
            'fec_ingreso' => 'Fec Ingreso :',
            'fojas' => 'Fojas :',
            'resumen' => 'Resumen :',
            'remitente' => 'Remitente :',
            'observaciones' => 'Observaciones :',
        ];
    }
}
