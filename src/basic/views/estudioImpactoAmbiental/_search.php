<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstudioImpactoAmbientalSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudio-impacto-ambiental-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'reglon') ?>

    <?= $form->field($model, 'tipo_eia') ?>

    <?= $form->field($model, 'empresa') ?>

    <?= $form->field($model, 'referencia') ?>

    <?php // echo $form->field($model, 'consultor') ?>

    <?php // echo $form->field($model, 'localidad') ?>

    <?php // echo $form->field($model, 'nro_expte_mineria') ?>

    <?php // echo $form->field($model, 'fec_recepcion') ?>

    <?php // echo $form->field($model, 'tipo_expediente') ?>

    <?php // echo $form->field($model, 'sub_tipo_expediente') ?>

    <?php // echo $form->field($model, 'expediente_sma') ?>

    <?php // echo $form->field($model, 'mineral') ?>

    <?php // echo $form->field($model, 'fec_nota') ?>

    <?php // echo $form->field($model, 'numero_Nota') ?>

    <?php // echo $form->field($model, 'fojas_recepcion') ?>

    <?php // echo $form->field($model, 'nombre_area') ?>

    <?php // echo $form->field($model, 'nombre_yacimiento') ?>

    <?php // echo $form->field($model, 'nombre_proyecto') ?>

    <?php // echo $form->field($model, 'arrendatario') ?>

    <?php // echo $form->field($model, 'observaciones') ?>

    <?php // echo $form->field($model, 'revision') ?>

    <?php // echo $form->field($model, 'secto_Revision') ?>

    <?php // echo $form->field($model, 'fec_ultimo_pase') ?>

    <?php // echo $form->field($model, 'ultimo_tramite') ?>

    <?php // echo $form->field($model, 'ult_usu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
