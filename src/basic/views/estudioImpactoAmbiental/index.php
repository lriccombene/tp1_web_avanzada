<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EstudioImpactoAmbientalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estudio Impacto Ambientals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudio-impacto-ambiental-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Estudio Impacto Ambiental', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'reglon',
            'tipo_eia',
            'empresa',
            'referencia',
            //'consultor',
            //'localidad',
            //'nro_expte_mineria',
            //'fec_recepcion',
            //'tipo_expediente',
            //'sub_tipo_expediente',
            //'expediente_sma',
            //'mineral',
            //'fec_nota',
            //'numero_Nota',
            //'fojas_recepcion',
            //'nombre_area',
            //'nombre_yacimiento',
            //'nombre_proyecto',
            //'arrendatario',
            //'observaciones:ntext',
            //'revision',
            //'secto_Revision',
            //'fec_ultimo_pase',
            //'ultimo_tramite',
            //'ult_usu',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
