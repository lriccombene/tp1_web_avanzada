<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstudioImpactoAmbiental */

$this->title = 'Create Estudio Impacto Ambiental';
$this->params['breadcrumbs'][] = ['label' => 'Estudio Impacto Ambientals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estudio-impacto-ambiental-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
