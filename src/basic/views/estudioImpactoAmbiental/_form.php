<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EstudioImpactoAmbiental */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="estudio-impacto-ambiental-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'reglon')->textInput() ?>

    <?= $form->field($model, 'tipo_eia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'empresa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'referencia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'consultor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'localidad')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nro_expte_mineria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fec_recepcion')->textInput() ?>

    <?= $form->field($model, 'tipo_expediente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sub_tipo_expediente')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expediente_sma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mineral')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fec_nota')->textInput() ?>

    <?= $form->field($model, 'numero_Nota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fojas_recepcion')->textInput() ?>

    <?= $form->field($model, 'nombre_area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_yacimiento')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_proyecto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'arrendatario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observaciones')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'revision')->textInput() ?>

    <?= $form->field($model, 'secto_Revision')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fec_ultimo_pase')->textInput() ?>

    <?= $form->field($model, 'ultimo_tramite')->textInput() ?>

    <?= $form->field($model, 'ult_usu')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
