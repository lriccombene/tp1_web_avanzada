<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

?>

<h1>Formulario stock</h1>

<h3><?= $mensaje?></h3>

<?php
$form= ActiveForm::begin([
    "method" =>"post",
    "id" => "formulario",
    "enableClientValidation"=>true,
    "enableAjaxValidation"=> false
    ]);
?>

<div class ="form-group">
    <?= $form->field($model,"id")->input("text") ?>
</div>
<div class ="form-group">
    <?= $form->field($model,"id_producto")->input("text") ?>
</div>


<div class= "form-group">
    <?= $form ->field($model,"cantidad")->input("text") ?>
</div>
<div class= "form-group">
<?= $form->field($model,'fecha')->input("date") ?>
</div>

<?= Html::submitInput("enviar", ["class"=> "btn-primary"]) ?>



<?php
$form->end()
?>
