<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Localidad;
use app\models\TipoExpediente;
use app\models\Mineral;
use app\models\Area;

?>

<h1> Formulario eia </h1>
<h3><?= $mensaje ?></h3>


   <?php $form = ActiveForm::begin(); 

   ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
                    <div class="form-group">
                        <?= $form->field($model, "id")->input("text") ?>
                    </div>  
                    <div class="form-group">
                        <?= $form->field($model, "reglon")->input("text") ?>
                    </div>
		</div>
		<div class="col-md-6">
                    <div class="form-group">
                       <?php
                            $a= ['1' => 'actuaciones', '0' => 'EIA', '2' => 'NORMAL', '3' => 'ANP', 
                                '4' => 'Denuncias', '5' => 'EIA ABandono de pozo', 
                                '6' => 'EIA ADENDA', '7' => 'EIA Remediación', '8' => 'Incidentes', 
                                '9' => 'Pasivos ambientales','10' => 'PMOyt'];
                            echo $form->field($model, 'tipo_eia')->dropDownList($a,['Prompt'=>'Select Option']);
                        ?>
                    </div>
                    <div class="form-group">
                        <?= $form->field($model, "empresa")->input("text") ?>
                    </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
                     <div class="form-group">
                        <?= $form->field($model, "referencia")->input("text") ?>
                       </div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
                     <div class="form-group">
                        <?= $form->field($model, "consultor")->input("text") ?>
                    </div>
		</div>
	</div>
    	<div class="row">
		<div class="col-md-6">
                     <div class="form-group">
                         <?= $form->field($model, 'localidad')->dropDownList(
                            ArrayHelper::map(Localidad::find()->all(),'id','descripcion' ), 
                                            ['prompt' => 'Seleccione Uno']
                            );  ?>
  
                    </div>
                     <div class="form-group">
                        <?= $form->field($model, "fec_recepcion")->input("date") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "expediente_sma")->input("text") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "fec_nota")->input("date") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "numero_Nota")->input("text") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "fojas_recepcion")->input("text") ?>
                    </div> 
		</div>
		<div class="col-md-6">
                     <div class="form-group">
                        <?= $form->field($model, "nro_expte_mineria")->input("text") ?>
                    </div>
                     <div class="form-group">
                        <?= $form->field($model, 'tipo_expediente')->dropDownList(
                            ArrayHelper::map(TipoExpediente::find()->all(),'id','descripcion' ), 
                                            ['prompt' => 'Seleccione Uno']
                            );  ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, 'mineral')->dropDownList(
                            ArrayHelper::map(Mineral::find()->all(),'id','descripcion' ), 
                                            ['prompt' => 'Seleccione Uno']
                            );  ?>
                    </div>
                     <div class="form-group">
                         <?= $form->field($model, 'nombre_area')->dropDownList(
                            ArrayHelper::map(Area::find()->all(),'id','descripcion' ), 
                                            ['prompt' => 'Seleccione Uno']
                         );  ?>
                    </div>
                     <div class="form-group">
                        <?= $form->field($model, "nombre_yacimiento")->input("text") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "nombre_proyecto")->input("text") ?>
                    </div> 
                     <div class="form-group">
                        <?= $form->field($model, "arrendatario")->input("text") ?>
                    </div> 

		</div>
                <div class="form-group">
                   <?= $form->field($model, "observaciones")->input("text") ?>
               </div> 
                <div class="form-group">
                   <?= $form->field($model, "revision")->input("date") ?>
               </div> 
                <div class="form-group">
                   <?= $form->field($model, "secto_Revision")->input("text") ?>
               </div> 
               <div class="col-md-4">
                    <div class="form-group">
                           <?= $form->field($model, "fec_ultimo_pase")->input("date") ?>
                   </div> 
               </div>
               <div class="col-md-4">
                    <div class="form-group">
                        <?= $form->field($model, "ultimo_tramite")->input("text") ?>
                     </div> 
               </div>
               <div class="col-md-4">
                    <div class="form-group">
                       <?= $form->field($model, "ult_usu")->input("text") ?>
                   </div> 
               </div>
	</div>
	
</div>



<?= Html::submitInput("Guardar", ["class"=>"btn btn-primary"]) ?>
<?= Html::submitButton('Imprimir Pase ', ['name' => 'print_pase', 'value' => 'print_pase','class' => 'btn btn-primary']) ?>
<?= Html::submitButton('Imprimir adenda',[ 'name'=>'print_adenda', 'value' => 'print_adenda','class' => 'btn btn-primary']) ?>
<?php
    $form->end()
?>
