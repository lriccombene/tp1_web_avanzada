<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\data\Pagination;
use yii\widgets\LinkPager;

?>

<a href="<?= Url::toRoute("site/notasexpedientes") ?>">Nuevo...</a>

<h3>Lista de la notas y expedientes...</h3>

<h3><?= $mensaje ?></h3>

<?php
    $form = ActiveForm::begin([
        "method"=>"get",
        "action"=> Url::toRoute("site/notasexpediente_grilla"),
        "enableClientValidation"=>true
    ]);
?>

<div class="form-group">
    <?= $form->field($model, "query")->input("search") ?>
</div>

<?= Html::submitInput("Buscar", ["class"=>"btn btn-primary"]) ?>

<?php
    $form->end()
?>



<table class="table table-bordered">
    <tr>
        <th>
            Nro tramite:
        </th>
        <th>
            Estado:
        </th>
        <th>
            Resumen:
        </th>
        <th>
            Acciones
        </th>
    </tr>
    <?php foreach ($data as $row): ?>
    <tr>    
        <td><?= $row->nro_tramite ?></td>
        <td><?= $row->estado ?></td>
        <td><?= $row->resumen ?></td>
        <td><a href="<?= Url::toRoute(["site/delusuario", 'id'=>$row->id]) ?>">Eliminar</a>
            <a href="<?= Url::toRoute(["site/actualizarnotas_expediente", 'id'=>$row->id]) ?>">Editar</a>
        </td>
    </tr>
    <?php endforeach ?>

</table>
<?= LinkPager::widget([
        "pagination"=>$pages
    ])
?>
