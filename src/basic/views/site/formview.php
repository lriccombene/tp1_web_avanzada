<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\data\Pagination;
use yii\widgets\LinkPager;

?>

<a href="<?= Url::toRoute("site/validar") ?>">Nuevo...</a>

<h3>Lista de la tabla usuarios...</h3>

<h3><?= $mensaje ?></h3>

<?php
    $form = ActiveForm::begin([
        "method"=>"get",
        "action"=> Url::toRoute("site/formview"),
        "enableClientValidation"=>true
    ]);
?>

<div class="form-group">
    <?= $form->field($model, "query")->input("search") ?>
</div>

<?= Html::submitInput("Buscar", ["class"=>"btn btn-primary"]) ?>

<?php
    $form->end()
?>



<table class="table table-bordered">
    <tr>
        <th>
            Código:
        </th>
        <th>
            Nombre:
        </th>
        <th>
            Email:
        </th>
        <th>
            Acciones
        </th>
    </tr>
    <?php foreach ($data as $row): ?>
    <tr>    
        <td><?= $row->id ?></td>
        <td><?= $row->nombre ?></td>
        <td><?= $row->email ?></td>
        <td><a href="<?= Url::toRoute(["site/delusuario", 'id'=>$row->id]) ?>">Eliminar</a></td>
    </tr>
    <?php endforeach ?>

</table>
<?= LinkPager::widget([
        "pagination"=>$pages
    ])
?>
