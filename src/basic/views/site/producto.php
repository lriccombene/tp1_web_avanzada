<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<h1>Formulario producto</h1>

<h3><?= $mensaje?></h3>

<?php
$form= ActiveForm::begin([
    "method" =>"post",
    "id" => "formulario",
    "enableClientValidation"=>true,
    "enableAjaxValidation"=> false
    ]);
?>

<div class ="form-group">
    <?= $form->field($model,"id")->input("text") ?>
</div>

<div class="form-group">
    <?= $form ->field($model,"nombre")->input("text")?>
</div>
<div class= "form-group">
<?= $form ->field($model,"detalle")->input("text")?>
</div>
<div class= "form-group">
    <?= $form ->field($model,"codigo")->input("text") ?>
</div>
<div class= "form-group">
    <?= $form->field($model,"cantidad")->input("text") ?>
</div>

<?= Html::submitInput("enviar", ["class"=> "btn-primary"]) ?>



<?php
$form->end()
?>
