<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\data\Pagination;
use yii\widgets\LinkPager;

?>

<a href="<?= Url::toRoute("site/estudioimpactoambiental") ?>">Nuevo...</a>

<h3>Lista estudio de impacto ambiental.</h3>

<h3><?= $mensaje ?></h3>

<?php
    $form = ActiveForm::begin([
        "method"=>"get",
        "action"=> Url::toRoute("site/estudioimpactoambiental_grilla"),
        "enableClientValidation"=>true
    ]);
?>

<div class="form-group">
    <?= $form->field($model, "query")->input("search") ?>
</div>

<?= Html::submitInput("Buscar", ["class"=>"btn btn-primary"]) ?>

<?php
    $form->end()
?>



<table class="table table-bordered">
    <tr>
        <th>
            referencia:
        </th>
        <th>
            consultor:
        </th>
        <th>
            localidad:
        </th>
        <th>
            Acciones
        </th>
    </tr>
    <?php foreach ($data as $row): ?>
    <tr>    
        <td><?= $row->referencia ?></td>
        <td><?= $row->consultor ?></td>
        <td><?= $row->localidad ?></td>
        <td><a href="<?= Url::toRoute(["site/", 'id'=>$row->id]) ?>">Eliminar</a>
            <a href="<?= Url::toRoute(["site/actualizar_estudioimpactoambiental", 'id'=>$row->id]) ?>">Editar</a>
        </td>
    </tr>
    <?php endforeach ?>

</table>
<?= LinkPager::widget([
        "pagination"=>$pages
    ])
?>
