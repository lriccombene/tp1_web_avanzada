<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
                        use app\models\Estado;
use app\models\Destinatario;

/* @var $this yii\web\View */
/* @var $model app\models\EstudioImpactoAmbiental */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Estudio Impacto Ambientals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="estudio-impacto-ambiental-view">

    <h1>Pases de expedientes Renglon : <?= Html::encode($model->reglon) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'reglon',
            'empresa',
            'referencia',
            'consultor',
            'localidad',
            'nro_expte_mineria',
            'fec_recepcion',
            'fec_nota',
            'numero_Nota',
            'fojas_recepcion',
            'observaciones:ntext',
            'revision',
            'secto_Revision',
            'fec_ultimo_pase',
            'ultimo_tramite',
            'ult_usu',
        ],
    ]) ?>

</div>


    <div class="SeguimientoTramiteForm">
        <div class="container-fluid">
            <?php $formFour = ActiveForm::begin();
            $formFour->action= yii\helpers\Url::to('site\SeguimientoTramite');
            ?>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">        
                        <?= $formFour->field($model2, 'id_nota_expet')->input("text") ?>
                   </div> 
                </div> 
                <div class="col-md-3"> 
                    <div class="form-group">        
                    <?= $formFour->field($model2, 'id_destinatario')->dropDownList(
                        ArrayHelper::map(Destinatario::find()->all(),'id','descripcion' ), 
                                        ['prompt' => 'Seleccione Uno']
                        );  ?>
                    </div>
                </div>
                 <div class="col-md-3">
                    <div class="form-group">
                       <?= $formFour->field($model2, 'estado_tramite')->dropDownList(
                            ArrayHelper::map(Estado::find()->all(),'id','descripcion' ), 
                                    ['prompt' => 'Seleccione Uno']
                        );  ?>
                     </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= $formFour->field($model2, 'fecha_pase')->input("date") ?>
                    </div>
                </div>
                 <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
                 </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div><!-- SeguimientoTramiteForm -->




