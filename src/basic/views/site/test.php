<?php 
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
?>


<h1><?= Html::encode($variable) ?></h1>



<?php $form = ActiveForm::begin([   'id' => 'order-search-form',
    'method' => 'get']); ?>

    <div>
    <?= $form->field('test')->textInput() ?>
    </div>


    <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>



<?php ActiveForm::end(); ?>