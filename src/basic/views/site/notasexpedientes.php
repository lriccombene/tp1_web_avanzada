<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Estado;
use app\models\Destinatario;

/* @var $this yii\web\View */
/* @var $model app\models\NotasExpedientes */
/* @var $form ActiveForm 
<?= $form->field($model, 'id')->input("text")  ?>
*/
?>

<h3><?= $mensaje?></h3>

<div class="NotasExpedientes">

    <?php $form = ActiveForm::begin(); ?>
        
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'nro_tramite')->input("text")  ?>
                <?= $form->field($model, 'tramite_original')->input("text")  ?>
                <?= $form->field($model, 'estado')->dropDownList(
                    ArrayHelper::map(Estado::find()->all(),'id','descripcion' ), 
                                    ['prompt' => 'Seleccione Uno']
                );  ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'nro_expte_nota')->input("text")  ?>
                <?= $form->field($model, 'fec_expt')->input("date")  ?>
                <?= $form->field($model, 'fec_llegada_viedma')->input("date")  ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'caratula_expte_nuevo')->input("text")  ?>
                <?= $form->field($model, 'fec_ingreso')->input("date")  ?>
                <?= $form->field($model, 'fojas')->input("text")  ?>
            </div>
        </div>
    </div>
        <?= $form->field($model, 'resumen')->input("text")  ?>
        <?= $form->field($model, 'remitente')->input("text")  ?>
        <?= $form->field($model, 'observaciones')->input("text")  ?>
        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
            <?= Html::submitButton('Imprimir Pase ', ['name' => 'print_pase', 'value' => 'print_pase','class' => 'btn btn-primary']) ?>
            <?= Html::submitButton('Imprimir Encabezado',[ 'name'=>'print_encabezado', 'value' => 'print_encabezado','class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

    </div><!-- NotasExpedientes -->


    <div class="SeguimientoTramiteForm">
        <div class="container-fluid">
                        <?php $formFour = ActiveForm::begin([
        'method' => 'post',
        'action' => yii\helpers\Url::to(['site/seguimientotramite']),
])           ?>

            <div class="row">
                <div class="col-md-4">
                 
                    <!--  $formFour->field($model2, 'id_nota_expet',)->textInput()->hint('Please enter your name')->input('text') 
    -->
                    <?= $formFour->field($model2, 'id_nota_expet')->input('text') ?>
                  
                    <?= $formFour->field($model2, 'id_destinatario')->dropDownList(
                    ArrayHelper::map(Destinatario::find()->all(),'id','descripcion' ), 
                                    ['prompt' => 'Seleccione Uno']
                    );  ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model2, 'estado_tramite')->dropDownList(
                            ArrayHelper::map(Estado::find()->all(),'id','descripcion' ), 
                                    ['prompt' => 'Seleccione Uno']
                    );  ?>
 
                </div>
                <div class="col-md-4">
                    <?= $formFour->field($model2, 'fecha_pase')->input("date") ?>
                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div><!-- SeguimientoTramiteForm -->




