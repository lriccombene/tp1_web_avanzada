<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SeguimientoTramite */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguimiento-tramite-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_nota_expet')->textInput() ?>

    <?= $form->field($model, 'id_destinatario')->textInput() ?>

    <?= $form->field($model, 'estado_tramite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_pase')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
