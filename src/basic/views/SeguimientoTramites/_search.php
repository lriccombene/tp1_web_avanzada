<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SeguimientoTramitesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seguimiento-tramite-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_nota_expet') ?>

    <?= $form->field($model, 'id_destinatario') ?>

    <?= $form->field($model, 'estado_tramite') ?>

    <?= $form->field($model, 'fecha_pase') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
