<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SeguimientoTramite */
/* @var $form ActiveForm */
?>
<div class="SeguimientoTramiteForm">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'id_nota_expet') ?>
        <?= $form->field($model, 'id_destinatario') ?>
        <?= $form->field($model, 'estado_tramite') ?>
        <?= $form->field($model, 'fecha_pase') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- SeguimientoTramiteForm -->
