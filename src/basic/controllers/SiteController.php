<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Console;
use app\models\Validar;
use app\models\Producto;
use app\models\FrmValidar;
use app\models\Stock;
use app\models\TblUsuario;
use app\models\TblValidar;
use app\models\TblProducto;
use app\models\TblStock;
use app\models\Estado;
use app\models\SeguimientoTramite;
use app\models\NotasExpedientes;
use app\models\NotasExpedientesSearch;
use \app\models\EstudioImpactoAmbiental;


use app\models\Query;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\Pagination;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionHola($nav="Valor a recibir")
    {
        $variable = 'Valor a mostrar2';
        $mensaje = 'Mensaje...';
        $arreglo = ['Valor0', 'Valor1', 'Valor2', 'Valor3', 'Valor4', 'Valor5'];

        return $this->render('hola', 
            [
            'variable'=>$variable,
            'mostrar'=>$mensaje,
            'arreglo'=>$arreglo,
            'nav'=>$nav
            ]
        );
    }

    public function actionForm($mensaje=null)
    {
        return $this->render('form', ["mensaje"=>$mensaje]);
    }

    public function actionSform()
    {
        $mensaje=null;
         
        if (isset($_REQUEST["nombre"]))
        {
            $mensaje="El nombre es: ".$_REQUEST["nombre"];
        }
        return $this->redirect(['site/form', "mensaje"=>$mensaje]);
    }

    public function actionValidar()
    {
        $model = new Validar;
        
        $mensaje = null;
         
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = new TblValidar;
                $tbl->nombre = $model->nombre;
                $tbl->email = $model->email;
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado";
                    $model->nombre=null;
                    $model->email=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //consultas, calculos, etc
            }
            else
            {
                $model->getErrors();
            }
        }
        return $this->render('validar', ['model'=>$model, 'mensaje'=>$mensaje]);
    }



    public function actionProducto(){
        $model = new Producto();
        $mensaje = null;

        if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
            Yii::$app->request->format = Response :: FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()))
        {
            if($model->validate()){

                $tbl = new TblProducto;
                $tbl->nombre = $model->nombre;
                $tbl->codigo = $model->codigo;
                $tbl->cantidad = $model->cantidad;
                $tbl->detalle = $model->detalle;

                
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado aaaaaaaaaaaaa";
                    $model->nombre=null;
                    $model->detalle=null;
                    $model->codigo=null;
                    $model->cantidad=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //con


            }else{
                $model->getErros();
            }

        }
        return $this->render('producto', ['model'=>$model, 'mensaje'=>$mensaje]);
    }


    public function actionForm1($mensaje=null){

        return $this->render('form1',["mensaje"=>$mensaje]);


    }

    //aca recivimos el get del formulario
    public function actionSform1(){
        $datotxt=null;

        if (isset(is_REQUEST['campotxt']))
        {
            $datotxt= "el valor enviado el foirm es :". is_REQUEST['campotxt'];

        }

        return $this->redirect(['site/form1', "mensaje"=>$datotxt]);


    }

    public function actionFrmvalidar()
    {
        $model = new FrmValidar();
        $mensaje = null;

        if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
            Yii::$app->request->format = Response :: FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()))
        {
            if($model->validate()){
                
                $tbl = new TblUsuario;
                $tbl->nombre = $model->nombre;
                $tbl->email = $model->email;
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado";
                    $model->nombre=null;
                    $model->email=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //con


            }else{
                $model->getErros();
            }

        }
        return $this->render('frmvalidar', ['model'=>$model, 'mensaje'=>$mensaje]);
    }
    public function actionStock(){
        $model = new Stock();
        $mensaje = null;

        if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
            Yii::$app->request->format = Response :: FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if($model->load(Yii::$app->request->post()))
        {
            if($model->validate()){

                $tbl = new TblStock;
                $tbl->id_producto = $model->id_producto;
                $tbl->cantidad = $model->cantidad;
                $tbl->fecha = $model->fecha;

                
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado ";
                    $model->id_producto=null;
                    $model->fecha=null;
                    $model->cantidad=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
            
                
                $mensaje="Su email fue guardado aaaaaaaaaaaaa";
            }else{
                $model->getErros();
            }

        }
        return $this->render('stock', ['model'=>$model, 'mensaje'=>$mensaje]);
    }
    
    public function actionFormview($mensaje=null)
    {
        $model = new Query;

        if ($model->load(Yii::$app->request->get()))
        {
            if ($model->validate())
            {
                $search = Html::encode($model->query);
                $table = TblValidar::find()
                        ->where(['like', 'id', $search])
                        ->orWhere(['like', 'nombre', $search])
                        ->orWhere(['like', 'email', $search]);
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $table = TblValidar::find();
        }
        $count = clone $table;
        $pages = new Pagination(
            [
                "pageSize"=>5,
                "totalCount"=> $count->count()
            ]);
        
        $data = $table->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        return $this->render('formview', 
                ["data"=>$data, 
                "model"=>$model , 
                'mensaje'=>$mensaje,
                'pages'=>$pages]);
    }

    public function actionDelusuario($id=null)
    {
        $mensaje = null;
        /*
        if (Yii::$app->user->isGuest)
        {
            $mensaje="No permitido...";
        }
        else
        {
        */
            $usr = TblValidar::findOne($id);
            $usr->delete();
        /*}*/
        return $this->redirect(['site/formview', "mensaje"=>$mensaje]);
    }

    public function actionNotasexpedientes()
    {
        $model = new NotasExpedientes;
        $mensaje = null;

        if(isset($_POST['print_pase']))
		{
            $model = new TblValidar;
            return $this->render('validar', ['model'=>$model, 'mensaje'=>$mensaje]);
        }
        
        if(isset($_POST['print_encabezado']))
		{
            $model = new TblValidar;
            return $this->render('validar', ['model'=>$model, 'mensaje'=>$mensaje]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()){
                $tbl = new NotasExpedientes;
                $tbl->nro_tramite = $model->nro_tramite;
                $tbl->tramite_original = $model->tramite_original;
                $tbl->estado = $model->estado;
                $tbl->nro_expte_nota= $model->nro_expte_nota;
                $tbl->fec_expt = $model->fec_expt;
                $tbl->fec_llegada_viedma = $model->fec_llegada_viedma;
                $tbl->caratula_expte_nuevo= $model->caratula_expte_nuevo;
                $tbl->fec_ingreso = $model->fec_ingreso;
                $tbl->fojas = $model->fojas;
                $tbl->resumen = $model->resumen;
                $tbl->remitente = $model->remitente;
                $tbl->observaciones = $model->observaciones;
                  
                if($tbl->insert())
                {
                    $mensaje="Su email fue guardado ";
                     $model->nro_tramite=$model->nro_tramite;
                     $model->tramite_original= $model->tramite_original;
                    $model->estado;
                     $model->nro_expte_nota= $model->nro_expte_nota;
                     $model->fec_expt;
                   $model->fec_llegada_viedma;
                     $model->caratula_expte_nuevo;
                     $model->fec_ingreso;
                     $model->fojas;
                     $model->resumen;
                     $model->remitente;
                     $model->observaciones;

                    //$model->fecha=null;
                    //$model->cantidad=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                            
                $mensaje="Su Nota expedientes  fue guardado aaaaaaaaaaaaa";

            }else{
                $model->getErros();
            }

        }
        $model2= new SeguimientoTramite;
        $model2->id_nota_expet = $model->nro_tramite;
        return $this->render('notasexpedientes', [
            'model' => $model,"mensaje"=>$mensaje,'model2' => $model2
        ]);
    }

    public function actionSeguimientotramite()
    {

        $mensaje = null;
        $model = new SeguimientoTramite();
        
         
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model );
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = new SeguimientoTramite();
                $tbl->id_nota_expet = $model->id_nota_expet;
                $tbl->id_destinatario = $model->id_destinatario;
                $tbl->estado_tramite = $model->estado_tramite;
                $tbl->fecha_pase = $model->fecha_pase;
                if ($tbl->insert())
                {
                    $mensaje="Su el pase fue guardado";
                    $model->id_nota_expet=null;
                    $model->id_destinatario=null;
                    $model->estado_tramite=null;
                    $model->fecha_pase=null;
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //consultas, calculos, etc
            }
            else
            {
                $model->getErrors();
            }
        }
        $model2=$model;
        $model= new NotasExpedientes;
        return $this->render('notasexpedientes', [
            'model' => $model,"mensaje"=>$mensaje,'model2' => $model2
        ]);
    }


    
    public function actionNotasexpediente_grilla($mensaje=null)
    {
        $model = new Query;

        if ($model->load(Yii::$app->request->get()))
        {
            if ($model->validate())
            {
                $search = Html::encode($model->query);
                $table = NotasExpedientes::find()
                        ->where(['like', 'id', $search])
                        ->orWhere(['like', 'nro_tramite', $search])
                        ->orWhere(['like', 'resumen', $search]);
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $table = NotasExpedientes::find();
        }
        $count = clone $table;
        $pages = new Pagination(
            [
                "pageSize"=>5,
                "totalCount"=> $count->count()
            ]);
        
        $data = $table->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        return $this->render('notasexpediente_grilla', 
                ["data"=>$data, 
                "model"=>$model , 
                'mensaje'=>$mensaje,
                'pages'=>$pages]);
    }


    public function actionActualizarnotas_expediente($id = null)
    {
        $model = new NotasExpedientes;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = NotasExpedientes::findOne($id);
                $tbl->nro_tramite = $model->nro_tramite;
                $tbl->tramite_original = $model->tramite_original;
                $tbl->estado = $model->estado;
                $tbl->nro_expte_nota= $model->nro_expte_nota;
                $tbl->fec_expt = $model->fec_expt;
                $tbl->fec_llegada_viedma = $model->fec_llegada_viedma;
                $tbl->caratula_expte_nuevo= $model->caratula_expte_nuevo;
                $tbl->fec_ingreso = $model->fec_ingreso;
                $tbl->fojas = $model->fojas;
                $tbl->resumen = $model->resumen;
                $tbl->remitente = $model->remitente;
                $tbl->observaciones = $model->observaciones;
                $tbl->update();
                $mensaje = "Se guardaron correctamente los datos de: ".$tbl->estado;
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $tbl = NotasExpedientes::findOne($id);

            $tbl->nro_tramite = $model->nro_tramite;
            $tbl->tramite_original = $model->tramite_original;
            $tbl->estado = $model->estado;

            $mensaje = "Modificando el Usuario: ".$tbl->estado;
        }   
        $model2= new SeguimientoTramite;
        return $this->render('notasexpedientes', [
            'model' => $model,"mensaje"=>$mensaje,'model2' => $model2,
        ]);
    }
    
    
    public function actionEstudioimpactoambiental_grilla($mensaje=null)
    {
        $model = new Query;

        if ($model->load(Yii::$app->request->get()))
        {
            if ($model->validate())
            {
                $search = Html::encode($model->query);
                $table = Estudioimpactoambiental::find()
                        ->where(['like', 'id', $search])
                        ->orWhere(['like', 'referencia', $search])
                        ->orWhere(['like', 'consultor', $search]);
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $table = Estudioimpactoambiental::find();
        }
        $count = clone $table;
        $pages = new Pagination(
            [
                "pageSize"=>5,
                "totalCount"=> $count->count()
            ]);
        
        $data = $table->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();

        return $this->render('estudioimpactoambiental_grilla', 
                ["data"=>$data, 
                "model"=>$model , 
                'mensaje'=>$mensaje,
                'pages'=>$pages]);
    }

    public function actionEstudioimpactoambiental()
    {
        $model = new EstudioImpactoAmbiental;
        
        $mensaje = null;
         
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = new EstudioImpactoAmbiental;
                $tbl->reglon = $model->reglon;
                $tbl->empresa = $model->empresa;
                $tbl->tipo_eia = $model->tipo_eia;
                $tbl->referencia = $model->referencia;
                $tbl->consultor = $model->consultor;
                $tbl->localidad = $model->localidad;
                $tbl->nro_expte_mineria = $model->nro_expte_mineria;
                $tbl->fec_recepcion = $model->fec_recepcion;
                $tbl->tipo_expediente = $model->tipo_expediente;
                $tbl->sub_tipo_expediente = "1";
                $tbl->expediente_sma = $model->expediente_sma;
                $tbl->mineral = $model->mineral;
                $tbl->fec_nota = $model->fec_nota;
                $tbl->numero_Nota = $model->numero_Nota;
                $tbl->fojas_recepcion = $model->fojas_recepcion;
                $tbl->nombre_area = $model->nombre_area;
                $tbl->nombre_yacimiento = $model->nombre_yacimiento;
                $tbl->nombre_proyecto = $model->nombre_proyecto;
                $tbl->arrendatario = $model->arrendatario;
                $tbl->observaciones = $model->observaciones;
                $tbl->revision = $model->revision;
                $tbl->secto_Revision = $model->secto_Revision;
                $tbl->fec_ultimo_pase = $model->fec_ultimo_pase;
                $tbl->ultimo_tramite = $model->ultimo_tramite;
                $tbl->ult_usu = $model->ult_usu;
                
                if ($tbl->insert())
                {
                    $mensaje="Su email fue guardado";
                    $model->empresa=null;
                    $model->reglon=null;
                    $model->tipo_eia=null;
                    $model->referencia=null;
                    $model->consultor=null;
                    $model->localidad=null;
                    $model->nro_expte_mineria=null;
                    $model->fec_recepcion=null;
                    $model->tipo_expediente=null;
                    $model->sub_tipo_expediente=null;
                    $model->expediente_sma=null;
                    $model->mineral=null;
                    $model->fec_nota=null;
                    $model->numero_Nota=null;
                    $model->fojas_recepcion=null;
                    $model->nombre_area=null;
                    $model->nombre_yacimiento=null;
                    $model->arrendatario=null;
                    $model->observaciones=null;
                    $model->revision=null;
                    $model->secto_Revision=null;
                    $model->fec_ultimo_pase=null;
                    $model->ultimo_tramite=null;
                    $model->ult_usu=null;
        
                }
                else
                {
                    $mensaje="Ha ocurrido un Error al insertar";
                }
                //consultas, calculos, etc
            }
            else
            {
                $model->getErrors();
            }
        }
         return $this->render('estudioimpactoambiental', ['model'=>$model, 'mensaje'=>$mensaje]);
    }
    
  
    public function actionActualizar_Estudioimpactoambiental($id = null)
    {
        $model = new EstudioImpactoAmbiental;
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
        {
            //consultas, calculos, etc
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
            if ($model->validate())
            {
                $tbl = EstudioImpactoAmbiental::findOne($id);
                $tbl->nro_tramite = $model->nro_tramite;
                $tbl->tramite_original = $model->tramite_original;
                $tbl->estado = $model->estado;
                $tbl->nro_expte_nota= $model->nro_expte_nota;
                $tbl->fec_expt = $model->fec_expt;
                $tbl->fec_llegada_viedma = $model->fec_llegada_viedma;
                $tbl->caratula_expte_nuevo= $model->caratula_expte_nuevo;
                $tbl->fec_ingreso = $model->fec_ingreso;
                $tbl->fojas = $model->fojas;
                $tbl->resumen = $model->resumen;
                $tbl->remitente = $model->remitente;
                $tbl->observaciones = $model->observaciones;
                $tbl->update();
                $mensaje = "Se guardaron correctamente los datos de: ".$tbl->estado;
            }
            else
            {
                $model->getErrors();
            }
        }
        else
        {
            $tbl = EstudioImpactoAmbiental::findOne($id);

            $tbl->nro_tramite = $model->nro_tramite;
            $tbl->tramite_original = $model->tramite_original;
            $tbl->estado = $model->estado;

            $mensaje = "Modificando el Usuario: ".$tbl->estado;
        }   
        $model2= new SeguimientoTramite;
        return $this->render('notasexpedientes', [
            'model' => $model,"mensaje"=>$mensaje,'model2' => $model2,
        ]);
    }
    
        //visualizo con una vista que una grilla 
    public function actionPasesexpedientes($id=1)
    {   

        $model = EstudioImpactoAmbiental::findOne($id);
        
        $model2 = new SeguimientoTramite;
        $model2->id_nota_expet= $model->reglon;
        return $this->render('pasesexpedientes', [
            'model' => $model, 'model2' => $model2,
        ]);
    }
}

